import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Date;
abstract class Operation
{
    int first;//第一位加数
    int last;//第二位加数
    int result;//正确答案
    int user;//用户答案
    int range;//范围
    int rem;//余数
    int userrem;//用户余数
    char sign;//运算符标志
    Operation(char a)
    {
        range=1;
        for(int j=0;j<a-48;j++)
        {
            range=range*10;
        }
    }
    boolean right()//用户答案是否正确
    {
        if(user==result)
        {
            return true;
        }
        else
            return false;
    }
    void printRes()//打印最终答案
    {

        System.out.print(first+"\t"+sign+"\t"+last+"\t"+"="+"\t"+result+"\t"+user+"\t"+"\t"+"\t");
        if(right()==true)
        {
            System.out.println("R");
        }
        else
        {
            System.out.println("F");
        }
    }
    void print()//打印题目
    {
        System.out.println(first+"\t"+sign+"\t"+last+"\t"+"=");
    }
    void setUser(int user)//为user赋值
    {
        this.user=user;
    }

    void userRem(int userrem)//为userrem赋值
    {
        this.userrem=userrem;

    }
}
class Addition extends Operation
{
    Addition(char a,Random number)
    {
        super(a);
        //随机生成加数
        first=number.nextInt(range*2-1)-range+1;
        last=number.nextInt(range*2-1)-range+1;
        //直接得到结果
        result=first+last;
        sign='+';
    }

}
class Subtraction extends Operation
{
    Subtraction(char a,Random number)
    {
        super(a);
        last=number.nextInt(range*2)-range+1;
        int max=range;
        int min=last;
        first=number.nextInt(max)%(max-min+1) + min;
        result=first-last;
        sign='-';
    }
}
class Multiplication extends Operation
{
    Multiplication(char a,Random number)
    {
        super(a);
        //随机生成乘数
        first=number.nextInt(range*2-1)-range+1;
        last=number.nextInt(range*2-1)-range+1;
        //直接得到结果
        result=first*last;
        sign='*';
    }
}
class Division extends Operation
{
    Division(char a,Random number)
    {

        super(a);
        first=number.nextInt(range*2)-range+1;
        last=number.nextInt(range-1)+1;
        result=first/last;
        rem=first%last;
        sign='/';

    }
    boolean right()//用户答案是否正确
    {
        if(user==result && userrem==rem)
        {
            return true;
        }
        else
            return false;
    }
    void printRes()//打印最终答案
    {
        System.out.print(first+"\t"+"/"+"\t"+last+"\t"+"="+"\t"+result+"\t"+user+"\t"+rem+"\t"+userrem+"\t");
        if(right()==true)
        {
            System.out.println("R");
        }
        else
        {
            System.out.println("F");
        }
    }
    void userRem(int userrem)//为userrem赋值
    {
        this.userrem=userrem;

    }
    void print()//打印题目
    {
        System.out.println(first+"\t"+"/"+"\t"+last+"\t"+"=");
    }

}
class ArithmeticTest3
{
    public static void main(String [] args)throws IOException, InterruptedException
    {
        Random number=new Random(new Date().getTime());
        Operation test[]=new Operation[10];
        if(args[0].charAt(0)=='+')
        {
            for(int i=0;i<10;i++)
            {
                test [i]= new Addition(args[0].charAt(1), number);
                test[i].print();
            }
        }
        else if(args[0].charAt(0)=='-')
        {
            for(int i=0;i<10;i++)
            {
                test [i]= new Subtraction(args[0].charAt(1), number);
                test[i].print();
            }
        }
        else if(args[0].charAt(0)=='/')
        {
            for(int i=0;i<10;i++)
            {
                test [i]= new Division(args[0].charAt(1), number);
                test[i].print();
            }
        }

        else if(args[0].charAt(0)=='*')
        {
            for(int i=0;i<10;i++)
            {
                test [i]= new Multiplication(args[0].charAt(1), number);
                test[i].print();
            }
        }
        else if(args[0].charAt(0)=='r')
        {
            int ran;
            for(int i=0;i<10;i++)
            {
                ran=number.nextInt(4);
                if(ran==0)
                {
                    test[i]=new Addition(args[0].charAt(1), number) ;
                }
                if(ran==1)
                {
                    test[i]=new Subtraction(args[0].charAt(1), number) ;
                }
                if(ran==2)
                {
                    test[i]=new Division(args[0].charAt(1), number) ;
                }
                if(ran==3)
                {
                    test[i]=new Multiplication(args[0].charAt(1), number) ;
                }
                test[i].print();
            }
        }
        //初始化分数
        int score=0;
        for(int i=0;i<10;i++)
        {
            int j=i+1;
            System.out.println("第"+j+"题：");
            BufferedReader keyboardIn = new BufferedReader(new InputStreamReader(System.in));
            int answer=Integer.parseInt(keyboardIn.readLine());
            test[i].setUser(answer);
            if(test[i].sign=='/')
            {
                System.out.println("输入余数：");
                BufferedReader keyboardIn1 = new BufferedReader(new InputStreamReader(System.in));
                int answer1=Integer.parseInt(keyboardIn.readLine());
                test[i].userRem(answer1);
            }
        }
        for(int i=0;i<10;i++)
        {
            if(test[i].right()==true)
            {
                score++;
            }
            test[i].printRes();
        }
        System.out.println("得分："+score);
    }
}